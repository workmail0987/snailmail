package com.example.snailmail;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.snailmail.Activities.AddressActivity;
import com.example.snailmail.util.SendMailTask;

import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final TextView btnGoNext = (TextView) this.findViewById(R.id.txt_go_next);
        EditText emailBody = findViewById(R.id.txt_letter);
        emailBody.requestFocus();
        btnGoNext.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                if (!emailBody.getText().toString().isEmpty()) {

                    String letter = emailBody.getText().toString().replaceAll("\\n","<br>");
                    SharedPreferences.Editor preference = getSharedPreferences("letterDetails", MODE_PRIVATE).edit();
                    preference.putString("letter", letter);
                    preference.commit();
                    startActivity(new Intent(MainActivity.this, AddressActivity.class));
                } else {
                    emailBody.requestFocus();
                    Toast.makeText(MainActivity.this, "Letter should not be empty", Toast.LENGTH_SHORT).show();
                }

//                new SendMailTask(MainActivity.this).execute(fromEmail,
//                        fromPassword, toEmailList, emailSubject, emailBody);
            }
        });
    }
}