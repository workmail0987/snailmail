package com.mailsail.snailmail;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.mailsail.snailmail.Activities.AddressActivity;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class MainActivity extends AppCompatActivity {
    private File filePath = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final TextView btnGoNext = (TextView) this.findViewById(R.id.txt_go_next);
        EditText emailBody = findViewById(R.id.txt_letter);
        emailBody.requestFocus();
        btnGoNext.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                if (!emailBody.getText().toString().isEmpty()) {

                    String letter = emailBody.getText().toString().replaceAll("\\n", "\n");
//                    SharedPreferences.Editor preference = getSharedPreferences("letterDetails", MODE_PRIVATE).edit();
//                    preference.putString("letter", letter);
//                    preference.commit();
                    createWordFile(letter);
                    startActivity(new Intent(MainActivity.this, AddressActivity.class));
                } else {
                    emailBody.requestFocus();
                    Toast.makeText(MainActivity.this, "Letter should not be empty", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    public void createWordFile(String letter) {
        filePath = new File(getExternalFilesDir(null), "letter.docx");
        try {
            if (!filePath.exists()) {
                filePath.createNewFile();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            XWPFDocument xwpfDocument = new XWPFDocument();
            XWPFParagraph xwpfParagraph = xwpfDocument.createParagraph();
            XWPFRun xwpfRun = xwpfParagraph.createRun();

            if (letter.contains("\n")) {
                String[] lines = letter.split("\n");
                xwpfRun.setText(lines[0], 0); // set first line into XWPFRun
                for (int i = 1; i < lines.length; i++) {
                    // add break and insert new text
                    xwpfRun.addBreak();
                    xwpfRun.setText(lines[i]);
                }
            } else {
                xwpfRun.setText(letter, 0);
            }

            xwpfRun.setFontSize(16);


            FileOutputStream fileOutputStream = new FileOutputStream(filePath);
            xwpfDocument.write(fileOutputStream);

            if (fileOutputStream != null) {
                fileOutputStream.flush();
                fileOutputStream.close();
            }
            xwpfDocument.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}

