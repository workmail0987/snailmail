package com.mailsail.snailmail.Activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.billingclient.api.AcknowledgePurchaseParams;
import com.android.billingclient.api.AcknowledgePurchaseResponseListener;
import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.BillingResult;
import com.android.billingclient.api.ConsumeParams;
import com.android.billingclient.api.ConsumeResponseListener;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchasesResponseListener;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.api.SkuDetailsParams;
import com.android.billingclient.api.SkuDetailsResponseListener;
import com.mailsail.snailmail.R;
import com.mailsail.snailmail.util.SendMailTask;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.wallet.AutoResolveHelper;
import com.google.android.gms.wallet.IsReadyToPayRequest;
import com.google.android.gms.wallet.PaymentData;
import com.google.android.gms.wallet.PaymentDataRequest;
import com.google.android.gms.wallet.PaymentsClient;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AddressActivity extends AppCompatActivity {

    @BindView(R.id.txt_go_next2)
    TextView txt_goNext;
    @BindView(R.id.edt_receiverName)
    EditText edt_receiverName;
    @BindView(R.id.edt_receiverStreet)
    EditText edt_receiverStreet;
    @BindView(R.id.edt_receiverAddress)
    EditText edt_receiverAddress;
    @BindView(R.id.edt_senderName)
    EditText edt_senderName;
    @BindView(R.id.edt_senderStreet)
    EditText edt_senderStreet;
    @BindView(R.id.edt_senderAddress)
    EditText edt_senderAddress;
    @BindView(R.id.btn_pay)
    Button btn_pay;
    BillingClient billingClient;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address);
        ButterKnife.bind(this);
        billingClient = BillingClient.newBuilder(this)
                .enablePendingPurchases()
                .setListener(new PurchasesUpdatedListener() {
                    @Override
                    public void onPurchasesUpdated(@NonNull BillingResult billingResult, @Nullable List<Purchase> list) {
                        if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK && list != null) {
                            for (Purchase purchase : list) {
                                if (purchase.getPurchaseState() == Purchase.PurchaseState.PURCHASED &&
                                        !purchase.isAcknowledged()) {
                                    Toast.makeText(AddressActivity.this, "Purchased", Toast.LENGTH_SHORT).show();
                                    verifyPurchase(purchase);
                                    handlePaymentSuccess();
                                }

                            }
                        }
                    }
                }).build();
        connectToGooglePlayBilling();

    }

    private void connectToGooglePlayBilling() {
        billingClient.startConnection(
                new BillingClientStateListener() {
                    @Override
                    public void onBillingServiceDisconnected() {
                        connectToGooglePlayBilling();
                    }

                    @Override
                    public void onBillingSetupFinished(@NonNull BillingResult billingResult) {
                        if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {
                            getProductDetails();
                        }
                    }
                }
        );
    }

    private void getProductDetails() {
        List<String> productIds = new ArrayList<>();
        productIds.add("send_mail_consumeable");
        SkuDetailsParams getProductDetailsQuery = SkuDetailsParams
                .newBuilder()
                .setSkusList(productIds)
                .setType(BillingClient.SkuType.INAPP)
                .build();
        Activity activity = this;
        billingClient.querySkuDetailsAsync(
                getProductDetailsQuery,
                new SkuDetailsResponseListener() {
                    @Override
                    public void onSkuDetailsResponse(@NonNull BillingResult billingResult, @Nullable List<SkuDetails> list) {
                        if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK &&
                                list != null) {

                            SkuDetails itemInfo = list.get(0);

                            btn_pay.setText("pay " + itemInfo.getPrice() + " to send");
                            btn_pay.setOnClickListener(
                                    new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            if (isValid(edt_receiverName.getText().toString(),
                                                    edt_receiverStreet.getText().toString(),
                                                    edt_receiverAddress.getText().toString(),
                                                    edt_senderName.getText().toString(),
                                                    edt_senderStreet.getText().toString(),
                                                    edt_senderAddress.getText().toString())) {
                                                SharedPreferences.Editor editor = getSharedPreferences("letterDetails", MODE_PRIVATE).edit();
                                                editor.putString("receiverName", edt_receiverName.getText().toString());
                                                editor.putString("receiverStreet", edt_receiverStreet.getText().toString());
                                                editor.putString("receiverAddress", edt_receiverAddress.getText().toString());
                                                editor.putString("senderName", edt_senderName.getText().toString());
                                                editor.putString("senderStreet", edt_senderStreet.getText().toString());
                                                editor.putString("senderAddress", edt_senderAddress.getText().toString());
                                                editor.commit();
                                                billingClient.launchBillingFlow(
                                                        activity,
                                                        BillingFlowParams.newBuilder().setSkuDetails(itemInfo).build()
                                                );
                                            }
                                        }
                                    }
                            );
                        }
                    }
                }
        );
    }

    private void verifyPurchase(Purchase purchase) {

        ConsumeParams consumeParams = ConsumeParams.newBuilder().setPurchaseToken(purchase.getPurchaseToken()).build();
        billingClient.consumeAsync(
                consumeParams,
                new ConsumeResponseListener() {
                    @Override
                    public void onConsumeResponse(@NonNull BillingResult billingResult, @NonNull String s) {
                        if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {
                            Toast.makeText(AddressActivity.this, "Consumed!", Toast.LENGTH_LONG).show();
                        }
                    }
                }
        );
    }

    private void handlePaymentSuccess() {


        SharedPreferences preferences = getSharedPreferences("letterDetails", MODE_PRIVATE);
        if (preferences != null) {
            String receiverAddress = preferences.getString("receiverAddress", "");
            String senderStreet = preferences.getString("senderStreet", "");
            String senderName = preferences.getString("senderName", "");
            String senderAddress = preferences.getString("senderAddress", "");
            String receiverName = preferences.getString("receiverName", "");
            String receiverStreet = preferences.getString("receiverStreet", "");


            List<String> list = new ArrayList<>();
            list.add("quantumpush1@gmail.com");
            list.add("cabenefiel@gmail.com");

            //sending email
            new SendMailTask(AddressActivity.this).execute("mailingsnailmail@gmail.com",
                    "alialiali1", list, "LETTER", "TO:" +
                            "\n" + receiverName + "\n" + receiverStreet + " " + receiverAddress + "\n\n" + "From:" +
                            "\n" + senderName + "\n" + senderStreet + " " + senderAddress + "\n\n", new File(getExternalFilesDir(null), "letter.docx"));
        }
    }

    private boolean isValid(String receiverName, String receiverStreet, String receiverAddress,
                            String senderName, String senderStreet, String senderAddress) {
        if (receiverName.isEmpty() || receiverStreet.isEmpty() || receiverAddress.isEmpty()
                || senderName.isEmpty() || senderStreet.isEmpty() || senderAddress.isEmpty()) {
            Toast.makeText(this, "Please enter all details", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        billingClient.queryPurchasesAsync(
                BillingClient.SkuType.INAPP,
                new PurchasesResponseListener() {
                    @Override
                    public void onQueryPurchasesResponse(@NonNull BillingResult billingResult, @NonNull List<Purchase> list) {
                        if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {
                            for (Purchase purchase : list) {
                                if (purchase.getPurchaseState() == Purchase.PurchaseState.PURCHASED &&
                                        !purchase.isAcknowledged()) {
                                    verifyPurchase(purchase);
                                }
                            }
                        }
                    }
                }
        );
    }
}